class Directory {
    constructor() {
        this.employees = [];
    }
    async add(e) {
        this.employees.push(e);
    }
    async list() {
        return this.employees;
    }
    async find(name) {
        let e = this.employees.find(e => e.name == name);
        if(e) 
            return e;
        throw ({
            name: name,
            error: 'not found'
        });
    }
}

export default Directory;