class Employee {
    constructor(name, phone) {
        this.name = name;
        this.phone = phone;
        this.print = () => console.log(`Name: ${this.name} Phone: ${this.phone}`);
    }
}

export default Employee;