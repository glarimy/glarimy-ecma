"use strict"

import Directory from './Directory';
import Employee from './Employee';

async function init() {
    try {
        let dir = new Directory();

        await dir.add(new Employee("Krishna", 8677676));
        await dir.add(new Employee("Mohan", 7878));
        let employees = await dir.list();
        employees.forEach(e => e.print());
        (await dir.find('Mohan')).print();
    } catch (exp) {
        console.log(exp);
    }
}

init();