const p = 1000;
const t = 10;
const r = 2.5;

function interest(p, t, r){
    return p*t*r/100;
}

console.log(`Simple Interest = ${interest(p, t, r)}`);