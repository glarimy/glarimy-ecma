config = {
    entry: './src/index.js',
     
    output: {
       path:'/Users/glarimy/Professional/Engineering/glarimy-ecma/web/target',
       filename: 'index.js',
    },
     
    devServer: {
       inline: true,
       port: 8080
    },
     
    module: {
       loaders: [
          {
             test: /\.js?$/,
             exclude: /node_modules/,
             loader: 'babel-loader',
                 
             query: {
                presets: ['es2015']
             }
          }
       ]
    }
 }
 
 module.exports = config;
 