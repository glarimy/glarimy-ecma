export function list() {
    document.querySelector('#listLink').addEventListener('click', function () {
        document.querySelector('#listTable').style.display = 'block';
        document.querySelector('#addForm').style.display = 'none';

        fetch('employees.json').then(response => response.json().then(data => {
            data.employees.forEach((e, i) => {
                let row = document.createElement("tr");
                row.innerHTML = `<td>${i + 1}</td><td>${e.name}</td><td>${e.phone}</td>`;
                document.querySelector('#list').appendChild(row);
            });
        }));
    });
}

export function add() {
    document.querySelector('#addLink').addEventListener('click', function () {
        document.querySelector('#listTable').style.display = 'none';
        document.querySelector('#addForm').style.display = 'block';
    });
}