import { Observable, Subject, BehaviorSubject, ReplaySubject, AsyncSubject } from 'rxjs';

/*
function getQuoteNow(ticker) {
    if (ticker == null || ticker.length < 3)
        return ("Invalid ticker");
    let quote = Math.floor(Math.random() * 1000) + 1;
    return (`${ticker}: Rs. ${quote}`);
}

console.log("Sync/pull Call Mode");
console.log("Requesting for stock quote");
console.log("Requested for stock quote. Wait for updates....");
const quote = getQuoteNow("SBI");
console.log(quote);
*/
/*
function getQuoteUpdate(ticker) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (ticker == null || ticker.length < 3)
                reject("Invalid ticker");
            const quote = Math.floor(Math.random() * 1000) + 1;
            resolve(`${ticker}: Rs. ${quote}`);
        }, 1000);
    });
}

console.log("Async/push-single Mode")
console.log("Requesting for stock quote");
getQuoteUpdate("SBI")
    .then(update => console.log(update))
    .catch(error => console.log(error));
console.log("Requested for stock quote. Wait for updates....");
*/
/*
function getQuoteUpdates(ticker) {
    return new Observable((subscriber) => {
        if (ticker == null || ticker.length < 3)
            subscriber.error("Invalid ticker");
        let count = 0;
        const cmd = setInterval(() => {
            let quote = Math.floor(Math.random() * 1000) + 1;
            subscriber.next(`${ticker}: Rs. ${quote}`);
            count++;
            if (count > 3)
                subscriber.complete();
        }, 1000);
        return function unsubscribe(){
            clearInterval(cmd);
        }
    });
}

console.log("Async/push-many Mode")
console.log("Requesting for stock quote");
const o = getQuoteUpdates("SBI");
const subscription = o.subscribe({
    next: update => console.log(update),
    complete: () => {
        subscription.unsubscribe();
        console.log("Updates over")
    },
    error: msg => console.log(msg)
});
console.log("Requested for stock quote. Wait for updates....");;
*/
/*

function publishQuoteUpdates(ticker) {
    return new Observable((subject) => {
        if (ticker == null || ticker.length < 3)
            subject.error("Invalid ticker");
        let count = 0;
        const cmd = setInterval(() => {
            let quote = Math.floor(Math.random() * 1000) + 1;
            subject.next(`${ticker}: Rs. ${quote}`);
            count++;
            if (count > 3)
                subject.complete();
        }, 1000);
        return function unsubscribe() {
            clearInterval(cmd);
        }
    });
}

console.log("Async/push-many-to-many Mode")
console.log("Requesting for stock quote");
const subject = new Subject();
const s1 = subject.subscribe({
    next: update => console.log(`1: ${update}`),
    error: msg => console.log(`1: ${msg}`),
    complete: () => {
        s1.unsubscribe();
        console.log("1: Updates over")
    }
});
const s2 = subject.subscribe({
    next: update => console.log(`2: ${update}`),
    error: msg => console.log(`2: ${msg}`),
    complete: () => {
        s2.unsubscribe();
        console.log("2: Updates over")
    }
});
const s3 = subject.subscribe({
    next: update => console.log(`3: ${update}`),
    error: msg => console.log(`3: ${msg}`),
    complete: () => {
        s3.unsubscribe();
        console.log("3: Updates over")
    }
});

const o = publishQuoteUpdates("SBI");
o.subscribe(subject);
console.log("Requested for stock quote. Wait for updates....");
*/
/*
function publishQuoteUpdatesReliably(ticker) {
    return new Observable((subject) => {
        if (ticker == null || ticker.length < 3)
            subject.error("Invalid ticker");
        let count = 0;
        const cmd = setInterval(() => {
            console.log("count - " + count);
            let quote = Math.floor(Math.random() * 1000) + 1;
            subject.next(`${ticker}: Rs. ${quote}`);
            count++;
            if (count > 3)
                subject.complete();
        }, 2000);
        return function unsubscribe() {
            clearInterval(cmd);
        }
    });
}

console.log("Async/push-many-to-many-reliable Mode")
console.log("Requesting for stock quote");
const subject = new BehaviorSubject(0);
const s1 = subject.subscribe({
    next: update => console.log(`1: ${update}`),
    error: msg => console.log(`1: ${msg}`),
    complete: () => {
        s1.unsubscribe();
        console.log("1: Updates over")
    }
});
const s2 = subject.subscribe({
    next: update => console.log(`2: ${update}`),
    error: msg => console.log(`2: ${msg}`),
    complete: () => {
        s2.unsubscribe();
        console.log("2: Updates over")
    }
});

const o = publishQuoteUpdatesReliably("SBI");
o.subscribe(subject);
console.log("Requested for stock quote. Wait for updates....");

setTimeout(()=>{
    const s3 = subject.subscribe({
        next: update => console.log(`3: ${update}`),
        error: msg => console.log(`3: ${msg}`),
        complete: () => {
            s3.unsubscribe();
            console.log("3: Updates over")
        }
    });    
}, 5000);
*/
/*
function relayQuoteUpdates(ticker) {
    return new Observable((subject) => {
        if (ticker == null || ticker.length < 3)
            subject.error("Invalid ticker");
        let count = 0;
        const cmd = setInterval(() => {
            console.log("count - " + count);
            let quote = Math.floor(Math.random() * 1000) + 1;
            subject.next(`${ticker}: Rs. ${quote}`);
            count++;
            if (count > 3)
                subject.complete();
        }, 2000);
        return function unsubscribe() {
            clearInterval(cmd);
        }
    });
}

console.log("Async/push-many-to-many-relay Mode")
console.log("Requesting for stock quote");
const subject = new ReplaySubject(3);
const s1 = subject.subscribe({
    next: update => console.log(`1: ${update}`),
    error: msg => console.log(`1: ${msg}`),
    complete: () => {
        s1.unsubscribe();
        console.log("1: Updates over")
    }
});
const s2 = subject.subscribe({
    next: update => console.log(`2: ${update}`),
    error: msg => console.log(`2: ${msg}`),
    complete: () => {
        s2.unsubscribe();
        console.log("2: Updates over")
    }
});

const o = relayQuoteUpdates("SBI");
o.subscribe(subject);
console.log("Requested for stock quote. Wait for updates....");

setTimeout(()=>{
    const s3 = subject.subscribe({
        next: update => console.log(`3: ${update}`),
        error: msg => console.log(`3: ${msg}`),
        complete: () => {
            s3.unsubscribe();
            console.log("3: Updates over")
        }
    });    
}, 7000);
*/
/*
function finalQuoteUpdates(ticker) {
    return new Observable((subject) => {
        if (ticker == null || ticker.length < 3)
            subject.error("Invalid ticker");
        let count = 0;
        const cmd = setInterval(() => {
            console.log("count - " + count);
            let quote = Math.floor(Math.random() * 1000) + 1;
            subject.next(`${ticker}: Rs. ${quote}`);
            count++;
            if (count > 3)
                subject.complete();
        }, 2000);
        return function unsubscribe() {
            clearInterval(cmd);
        }
    });
}

console.log("Async/push-last-to-many Mode")
console.log("Requesting for stock quote");
const subject = new AsyncSubject();
const s1 = subject.subscribe({
    next: update => console.log(`1: ${update}`),
    error: msg => console.log(`1: ${msg}`),
    complete: () => {
        s1.unsubscribe();
        console.log("1: Updates over")
    }
});
const s2 = subject.subscribe({
    next: update => console.log(`2: ${update}`),
    error: msg => console.log(`2: ${msg}`),
    complete: () => {
        s2.unsubscribe();
        console.log("2: Updates over")
    }
});

const o = finalQuoteUpdates("SBI");
o.subscribe(subject);
console.log("Requested for stock quote. Wait for updates....");

setTimeout(()=>{
    const s3 = subject.subscribe({
        next: update => console.log(`3: ${update}`),
        error: msg => console.log(`3: ${msg}`),
        complete: () => {
            s3.unsubscribe();
            console.log("3: Updates over")
        }
    });    
}, 8000);
*/

function skipLargeQuotes(limit, ticker) {
    return (observable) => new Observable(observer => {
        const subscription = observable.subscribe({
            next(quote) {
                if (parseInt(quote) < parseInt(limit))
                    observer.next(`${ticker}: Rs. ${quote}`);
                else
                    console.log("skipping larger quote:" + quote);
            },
            error(err) {
                observer.error(err);
            },
            complete() {
                observer.complete();
            }
        });
        return () => {
            subscription.unsubscribe();
            console.log("cleaned");
        }
    });
}

function getQuoteUpdates(ticker) {
    return new Observable((subscriber) => {
        if (ticker == null || ticker.length < 3)
            subscriber.error("Invalid ticker");
        let count = 0;
        const cmd = setInterval(() => {
            let quote = Math.floor(Math.random() * 1000) + 1;
            subscriber.next(quote);
            count++;
            if (count > 3)
                subscriber.complete();
        }, 1000);
        return function unsubscribe() {
            clearInterval(cmd);
        }
    });
}

console.log("Async/push-many Mode")
console.log("Requesting for stock quote");
let o = getQuoteUpdates("SBI");
o = o.pipe(skipLargeQuotes(500, "SBI"));
const subscription = o.subscribe({
    next: update => console.log(update),
    complete: () => {
        subscription.unsubscribe();
        console.log("Updates over")
    },
    error: msg => console.log(msg)
});

console.log("Requested for stock quote. Wait for updates....");