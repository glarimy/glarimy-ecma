"use strict";

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Employee = function () {
    function Employee(eid, name, email) {
        _classCallCheck(this, Employee);

        this.eid = eid;
        this.name = name;
        this.email = email;
    }

    _createClass(Employee, [{
        key: "print",
        value: function print() {
            console.log("ID: " + this.eid + " | Name: " + this.name + " | Email: " + this.email);
        }
    }]);

    return Employee;
}();

var Manager = function (_Employee) {
    _inherits(Manager, _Employee);

    function Manager(eid, name, email, team) {
        _classCallCheck(this, Manager);

        var _this = _possibleConstructorReturn(this, (Manager.__proto__ || Object.getPrototypeOf(Manager)).call(this, eid, name, email));

        _this.team = team;
        return _this;
    }

    _createClass(Manager, [{
        key: "print",
        value: function print() {
            _get(Manager.prototype.__proto__ || Object.getPrototypeOf(Manager.prototype), "print", this).call(this);
            console.log("TEAM: " + this.team);
        }
    }]);

    return Manager;
}(Employee);

var Directory = function () {
    function Directory() {
        _classCallCheck(this, Directory);

        this.employees = {};
    }

    _createClass(Directory, [{
        key: "add",
        value: function add(employee) {
            if (employee instanceof Employee) {
                if (employee == undefined || employee.eid == undefined || employee.name == undefined || employee.email == undefined) throw "Invalid Employee";
                if (employee instanceof Manager) if (employee.team == undefined) throw "Invalid Manager";
                this.employees[employee.eid] = employee;
            } else {
                throw "Invalid Employee";
            }
        }
    }, {
        key: "find",
        value: function find(eid) {
            var employee = this.employees[eid];
            if (employee) return employee;
            throw "No employee is found with ID: " + eid;
        }
    }, {
        key: "print",
        value: function print() {
            console.log("EMPLOYEES");
            for (var eid in this.employees) {
                this.employees[eid].print();
            }
        }
    }]);

    return Directory;
}();

var directory = new Directory();
try {
    directory.add(new Employee("GTS0006", "Prabhu Deva Pangidi", "prabhudeva.pangidi@glarimy.com"));
    directory.add(new Manager("GTS0001", "Krishna Mohan Koyya", "krishna@glarimy.com", ["GTS0006"]));
    directory.add(new Manager("GTS0001", "Krishna Mohan Koyya", "krishna@glarimy.com"));
} catch (error) {
    console.log(error);
}
try {
    directory.print();
    directory.find("GTS0001").print();
} catch (error) {
    console.log(error);
}