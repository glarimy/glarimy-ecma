class PurchaseOrder {
    constructor(pono, poid, vendor, client, dates, duration, cost){
        this.pono = pono;
        this.vondor = vendor;
        this.poid = poid;
        this.client = client;
        this.dates = dates;
        this.duration = duration;
        this.cost = cost;
    }

    computePrice() {
        this.fee = this.duration * this.cost;
    }

    generatePDF() {
        let header = `PURCHAE ORDER`;
        let detail = `Client: ${this.client} and Vendor: ${this.vendor}`
        let price = `Price: Rs. ${this.fee} only`;
        console.log(`${header}
${detail}
${price}`);
    }
}

let firstpo = new PurchaseOrder(1, 34, "MK", "RBD", [], 3, 25000);
firstpo.computePrice();
firstpo.generatePDF();
