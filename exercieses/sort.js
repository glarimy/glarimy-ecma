'use strict'

const data = [3, 6, 2, 8, 10];

console.log(`Original Array: ${data}`);

for(let i=0; i<data.length; i++)
    for(let j=0; j<data.length; j++)
        if(data[j] > data[j+1]){
            let temp = data[j];
            data[j] = data[j+1];
            data[j+1] = temp;
        }

console.log(`Sorted Array: ${data}`);

