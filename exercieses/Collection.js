"use strict"

class Collection {
    sort(data, condition) {
        for (let i = 0; i < data.length; i++)
            for (let j = 0; j < data.length; j++)
                if (condition(data[j], data[j + 1])) {
                    let temp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp;
                }
    }

    filter(data, condition) {
        const result = [];
        for (let i = 0; i < data.length; i++)
            if (condition(data[i]))
                result.push(data[i]);
        return result;
    }

    map(data, transform) {
        const result = [];
        for (let i = 0; i < data.length; i++)
            result.push(transform(data[i]));
        return result;

    }
}

const data = [4, 7, 1, 0, 3];
const collection = new Collection();
console.log(`Before: ${data}`);
collection.sort(data, (first, second) => first > second);
console.log(`After : ${data}`);
console.log(collection.filter(data, e => e % 2 == 0));
console.log(collection.map(['one', 'two', 'three'], e => e.toUpperCase()));
