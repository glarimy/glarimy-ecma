class Calculator {
    constructor() {
        this.unit = 'millions';
    }
    diff(f, s) {
        return (parseInt(f) - parseInt(s))+this.unit;
    }
    sum(f, s) {
        return parseInt(f) + parseInt(s);
    }
}