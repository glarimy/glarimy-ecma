'use strict'

let array = ["10", "20", "30", "40"];

loopTheArray(array);

async function loopTheArray(array){
    // this loop moves to next iteration only after await resolution
    for(const e of array){
        console.log("calling async function");
        await doSomething(e);
    }
}

/*
async function loopTheArray(array) {
    // this loop moves to next iteration immediately after registering an await function
    array.forEach(async function(e){
        console.log("calling async function");
        await doSomething(e);
    });
}
*/

function doSomething(n) {
    return new Promise((resolve, reject) => {
        return setTimeout(() => {
            console.log("timeout of " + n + " milli seconds");
            resolve("done");
        }, n * 1000);
    });
}